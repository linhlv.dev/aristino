window.onscroll = function() {myFunction()};

var header = document.getElementById("headerMain");
var sticky = header.offsetTop;

function myFunction() {
    if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
    } else {
        header.classList.remove("sticky");
    }
}


$('.chooseSize-group>.btn-chooseSize').click(function(){
    $(this).parent().children(".chooseSize-drop").show();
})

$('.chooseSize-group .btn-close-poup').click(function(){
    $('.chooseSize-drop').hide();
})

$(document).on("click", function (event) {
    var $trigger = $(".chooseSize-group");
    if ($trigger !== event.target && !$trigger.has(event.target).length) {
        $(".chooseSize-drop").hide();
    }
});



/*Login*/
$('.showLogin').click(function () {
    $('#SibarLogin').toggleClass('open');
    $('body').append('<div class="modal-backdrop fade show"></div>')
});

$('.showRegister').click(function () {
    $('#SibarRegister').toggleClass('open');
    $('body').append('<div class="modal-backdrop fade show"></div>')
});

$('.SibarNavBar .btn-close').click(function () {
    $('.SibarNavBar').removeClass('open');
    $('.modal-backdrop').remove();
});


/*Search Box*/

$('.ClickToggleSearchTop').click(function(){
    $('.header_main__search').toggleClass('open');
})

// $('#SearchInputTop').click(function(){
//     if ($("#SearchInputTop").is(":focus")) {
//         $('.suggestion_box').addClass('show');
//       }
// })
